# Network Analysis

## Get Started

### :package: Dependencies Installation

```shell
$ pip install -r requirement.txt
```

### :beer: Run

```shell
$ python3 main.py
```
