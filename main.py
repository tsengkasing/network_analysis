# -*- coding: utf-8 -*-

import networkx as nx
import json
import csv
from os import path


def main():
    graphs = load_data()
    calculate_metrics(graphs)


def load_data():
    """Load data from file
    :return: graphs: dict
    """
    graphs = {}
    file_path = path.join(path.dirname(__file__), 'data', 'revision_relation_2016.csv')
    with open(file_path, encoding='utf-8', mode='r') as file:
        rows = csv.reader(file)
        for row in rows:
            group_id, year, school, grade, clazz, \
                group_no, clazz_name, from_id, from_name, \
                to_id, to_name, revision_count = row
            revision_count = int(revision_count)

            if group_id not in graphs:
                graphs[group_id] = {
                    'nodes': [from_name, to_name],
                    'edges': [(
                        from_name,
                        to_name,
                        {'weight': revision_count, 'distance': 1 / revision_count if revision_count != 0 else 10}
                    )]
                }
            else:
                graphs[group_id]['nodes'].append(from_name)
                graphs[group_id]['nodes'].append(to_name)
                graphs[group_id]['edges'].append((
                    from_name,
                    to_name,
                    {'weight': revision_count, 'distance': 1 / revision_count if revision_count != 0 else 10}
                ))
    for group_id in graphs:
        graphs[group_id]['nodes'] = list(set(graphs[group_id]['nodes']))  # remove duplicate ids
    return graphs


def calculate_metrics(graphs):
    centrality_result = []
    for group_id in graphs:
        nodes = graphs[group_id]['nodes']
        edges = graphs[group_id]['edges']
        graph = nx.DiGraph()  # nx.DiGraph()
        graph.add_nodes_from(nodes)
        graph.add_edges_from(edges)
        # graph.add_weighted_edges_from(edges)
        graph._weight = 'weight'

        temp_result = [[group_id, uname] for uname in nodes]

        # draw graph (optional)
        # draw_graph(graph, group_id)

        # Centrality
        # https://networkx.github.io/documentation/networkx-2.3/reference/algorithms/generated/networkx.algorithms.centrality.in_degree_centrality.html#networkx.algorithms.centrality.in_degree_centrality
        in_degree_centrality = nx.in_degree_centrality(graph, weight='weight')
        # https://networkx.github.io/documentation/networkx-2.3/reference/algorithms/generated/networkx.algorithms.centrality.out_degree_centrality.html#networkx.algorithms.centrality.out_degree_centrality
        out_degree_centrality = nx.out_degree_centrality(graph, weight='weight')
        # https://networkx.github.io/documentation/networkx-2.3/reference/algorithms/generated/networkx.algorithms.centrality.closeness_centrality.html#networkx.algorithms.centrality.closeness_centrality
        closeness_centrality = nx.closeness_centrality(graph, distance='distance')
        # https://networkx.github.io/documentation/networkx-2.3/reference/algorithms/generated/networkx.algorithms.centrality.betweenness_centrality.html#networkx.algorithms.centrality.betweenness_centrality
        betweenness_centrality = nx.betweenness_centrality(graph, weight='weight')

        for i in range(len(temp_result)):
            uname = temp_result[i][1]
            temp_result[i].append(in_degree_centrality[uname])
            temp_result[i].append(out_degree_centrality[uname])
            temp_result[i].append(closeness_centrality[uname])
            temp_result[i].append(betweenness_centrality[uname])

        # printing
        for kv in [
            ('in_degree_centrality', in_degree_centrality),
            ('out_degree_centrality', out_degree_centrality),
            ('closeness_centrality', closeness_centrality),
            ('betweenness_centrality', betweenness_centrality)
        ]:
            type_centrality, v = kv
            print('================ ' + group_id + ' | ' + type_centrality + ' ====================')
            print(json.dumps(v, indent=2))

        # Centralization
        N = graph.order()
        if N <= 1:  # If there is only ONE node in graph, no need to calculate centralization
            continue
        in_degree_values = [x[1] for x in graph.in_degree()]
        in_degree_centralization = (N * max(in_degree_values) - sum(in_degree_values)) / (N - 1) ** 2
        print('{} in degree centralization: {}'.format(group_id, in_degree_centralization))

        # writing to file "centrality.json"
        # centrality_result[group_id] = {
        #     'in_degree_centrality': in_degree_centrality,
        #     'out_degree_centrality': out_degree_centrality,
        #     'closeness_centrality': closeness_centrality,
        #     'betweenness_centrality': betweenness_centrality,
        # }

        # writing to file "centrality.csv"
        centrality_result.extend(temp_result)
    with open(path.join(path.dirname(__file__), 'output', 'centrality.csv'), encoding='utf-8', mode='w') as f:
        writer = csv.writer(f)
        writer.writerow([
            'group_id',
            'user_name',
            'in_degree_centrality',
            'out_degree_centrality',
            'closeness_centrality',
            'betweenness_centrality'
        ])
        writer.writerows(centrality_result)
        # json.dump(centrality_result, f, indent=4)


def draw_graph(graph, group_id):
    # need to install "graphviz"
    fig = nx.nx_agraph.to_agraph(graph)
    fig.draw(path.join(path.dirname(__file__), 'output', '{}.png'.format(group_id)), prog='dot')


if __name__ == '__main__':
    main()
