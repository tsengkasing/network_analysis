# "networkx" Version 2.3

```
pip install networkx==2.3
```



## in_degree

D:\Software\Anaconda3\Lib\site-packages\networkx\algorithms\centrality\degree_alg.py
- Line 58
```diff
- def in_degree_centrality(G):
+ def in_degree_centrality(G, weight=None):
```

- Line 96
```diff
- centrality = {n: d * s for n, d in G.in_degree()}
+ centrality = {n: d * s for n, d in G.in_degree(weight=weight)}
```

D:\Software\Anaconda3\Lib\site-packages\networkx\classes\digraph.py

- Line 986

```diff
- def in_degree(self):
+ def in_degree(self, weight=None):
```

- Line 1030

```diff
-return InDegreeView(self)
+ return InDegreeView(self, weight=weight)
```

## out_degree

D:\Software\Anaconda3\Lib\site-packages\networkx\algorithms\centrality\degree_alg.py

- Line 101
```diff
- def out_degree_centrality(G):
+ def out_degree_centrality(G, weight=None):
```

- Line 139
```diff
- centrality = {n: d * s for n, d in G.out_degree()}
+ centrality = {n: d * s for n, d in G.out_degree(weight=weight)}
```

D:\Software\Anaconda3\Lib\site-packages\networkx\classes\digraph.py
- Line 1033
```diff
- def out_degree(self):
+ def out_degree(self, weight=None):
```

- Line 1077
```diff
- return OutDegreeView(self)
+ return OutDegreeView(self, weight=weight)
```



## Usage

```python
import networkx as nx
# nodes => [1, 2]
# edges => 1 => 2, weight = 2

revision_count = 2
graph = nx.DiGraph()  # nx.DiGraph()
graph.add_nodes_from([1,2])
graph.add_edges_from([(
    1, 
    2, 
    {
        'weight': revision_count,
        'distance': 1 / revision_count
    }
)])

in_degree_centrality = nx.in_degree_centrality(graph, weight='weight')
out_degree_centrality = nx.out_degree_centrality(graph, weight='weight')
closeness_centrality = nx.closeness_centrality(graph, distance='distance')
betweenness_centrality = nx.betweenness_centrality(graph, weight='weight')
```

